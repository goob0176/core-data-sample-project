//
//  AHCourse+CoreDataProperties.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHCourse+CoreDataProperties.h"

@implementation AHCourse (CoreDataProperties)

+ (NSFetchRequest<AHCourse *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AHCourse"];
}

@dynamic name;
@dynamic students;
@dynamic teacher;

@end
