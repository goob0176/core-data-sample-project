//
//  AHCourse+CoreDataProperties.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHCourse+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AHCourse (CoreDataProperties)

+ (NSFetchRequest<AHCourse *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, retain) NSSet<AHStudent *> *students;
@property (nullable, nonatomic, retain) AHTeacher *teacher;

@end

@interface AHCourse (CoreDataGeneratedAccessors)

- (void)addStudentsObject:(AHStudent *)value;
- (void)removeStudentsObject:(AHStudent *)value;
- (void)addStudents:(NSSet<AHStudent *> *)values;
- (void)removeStudents:(NSSet<AHStudent *> *)values;

@end

NS_ASSUME_NONNULL_END
