//
//  AHCourse+CoreDataClass.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AHStudent, AHTeacher;

NS_ASSUME_NONNULL_BEGIN

@interface AHCourse : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "AHCourse+CoreDataProperties.h"
