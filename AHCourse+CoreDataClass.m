//
//  AHCourse+CoreDataClass.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHCourse+CoreDataClass.h"
#import "AHStudent+CoreDataClass.h"

#import "AHTeacher+CoreDataClass.h"

@implementation AHCourse

@end
