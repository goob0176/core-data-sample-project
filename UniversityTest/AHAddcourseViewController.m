//
//  AHAddcourseViewController.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHAddcourseViewController.h"

#import "AHStudent+CoreDataClass.h"
#import "AHCourse+CoreDataProperties.h"

#import "AHAddStudentsCell.h"

#import "AHDataManager.h"

@interface AHAddcourseViewController ()

@property (strong, nonatomic) NSString* courseName;
@property (strong, nonatomic) NSMutableSet* students;

@property (strong, nonatomic) NSMutableArray* studentsArray;

@end

typedef enum {
    
    AHSectionTypeStat,
    AHSectionTypeStudents
    
} AHSectionType;


@implementation AHAddcourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.students = [NSMutableSet set];
    
    self.studentsArray = [NSMutableArray array];
    
    [self.studentsArray addObjectsFromArray:[[AHDataManager sharedManager] getAllStudents]];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == AHSectionTypeStudents) {
        
        return @"Записать сдутентов на курс";
        
    } else {
        
        return nil;
        
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == AHSectionTypeStat) {
        
        return 1;
        
    }  else {
        
    return self.studentsArray.count;
        
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == AHSectionTypeStat) {
        
        static NSString* statIdentifier = @"statCell";
        
        AHAddStudentsCell *cell = [tableView dequeueReusableCellWithIdentifier:statIdentifier forIndexPath:indexPath];
        
        if (!cell) {
            
            cell = [[AHAddStudentsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:statIdentifier];
            
        }
        
        cell.setStatOfStudentTextField.tag  = indexPath.row;
        cell.setStatOfStudentTextField.delegate = self;
        [cell.setStatOfStudentTextField addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];

        
        return cell;
        
    } else {
        
        static NSString* studIdentifier = @"studCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:studIdentifier forIndexPath:indexPath];
        
        if (!cell) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:studIdentifier];
            
        }
        
        AHStudent* student = [self.studentsArray objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
        
        return cell;
        
    }
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == AHSectionTypeStudents) {
        
        AHStudent* student = [self.studentsArray objectAtIndex:indexPath.row];
        
        [self.students addObject: student];
        
       [self.studentsArray removeObject:student];
        
        [tableView reloadData];
        
    }
}

#pragma mark - UITextFieldDelegate


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
    
}

#pragma mark - Actions

- (IBAction)generateCourse:(UIBarButtonItem *)sender {
    
    
     NSCharacterSet *badSymbols = [NSCharacterSet characterSetWithCharactersInString:@"1234567890:<>@*!?#$%%^&-_+=±;/|`"];
    
    if ([self.courseName rangeOfCharacterFromSet:badSymbols].location == NSNotFound && ![self.courseName isEqualToString:@""]) {
        
        [[AHDataManager sharedManager] generateCourseWithName:self.courseName andStudents:self.students];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } else {
        
        [self createErrorAlert];
        
    }
    
}

- (IBAction)dismissController:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

#pragma mark - Methods

-(void) textFieldDidChange:(UITextField*) sender {
    
    self.courseName = sender.text;
    
}


-(void) createErrorAlert {
    
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Ошибка" message:[self errorMessageText] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* action = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleCancel handler:nil];
    
    [ac addAction:action];
    
    [self presentViewController:ac animated:YES completion:nil];
    
    
}

-(NSString*) errorMessageText {
    
    
    NSString* message;
    
    if (self.courseName == nil|| [self.courseName isEqualToString:@""]) {
        
        message = @"Пожалуйста, введите название курса!";
        
    } else {
        
        message = @"Используйте только буквы при заполнении строк";
        
    }
    
    return message;
    
}


@end
