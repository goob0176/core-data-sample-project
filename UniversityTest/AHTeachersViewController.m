//
//  AHTeachersViewController.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 08.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHTeachersViewController.h"

#import "AHStudent+CoreDataClass.h"
#import "AHStudent+CoreDataProperties.h"

#import "AHCourse+CoreDataClass.h"
#import "AHCourse+CoreDataProperties.h"

#import "AHTeacher+CoreDataClass.h"
#import "AHTeacher+CoreDataProperties.h"

#import "AHCoursesViewController.h"

@interface AHTeachersViewController ()

@end

@implementation AHTeachersViewController

@synthesize fetchedResultsController = _fetchedResultsController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    #pragma mark - NSFetchedResultsController
}

- (NSFetchedResultsController*)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // [[AHDataManager sharedManager] generateAndAddUniversuty];
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* desc = [NSEntityDescription entityForName:@"AHTeacher"
                                            inManagedObjectContext:self.managedObjectContext];
    
    
    [fetchRequest setEntity:desc];
    
    NSSortDescriptor* nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[nameDescriptor]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    _fetchedResultsController = aFetchedResultsController;
    return _fetchedResultsController;
}

#pragma mark - UITableViewDataSourse

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    
    return  [sectionInfo name];
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath: (NSIndexPath*) indexPath {
    
    AHTeacher* teacher = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text =  [NSString stringWithFormat:@"%@ %@", teacher.firstName, teacher.lastName];
}



#pragma mark - UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AHTeacher* teacher = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (teacher.courses.count == 0) {
        
        [self createEmptyCoursesAlertontroller];
        
    } else {
        
        AHCoursesViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"coursesController"];
        
        vc.teacher = teacher;
        
        [self showViewController:vc sender:self];
        
    }
    
}

#pragma mark - Methods 


-(void) createEmptyCoursesAlertontroller {
    
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle:@"Этот преподаватель не ведет ни один курс" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* action = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:action];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
