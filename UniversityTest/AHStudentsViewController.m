//
//  AHStudentsViewController.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHStudentsViewController.h"

#import "AHStudent+CoreDataClass.h"
#import "AHStudent+CoreDataProperties.h"

#import "AHStudentProfileController.h"

@interface AHStudentsViewController ()

@end

@implementation AHStudentsViewController

@synthesize fetchedResultsController = _fetchedResultsController;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}


- (NSFetchedResultsController*)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // [[AHDataManager sharedManager] generateAndAddUniversuty];
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* desc = [NSEntityDescription entityForName:@"AHStudent"
                                            inManagedObjectContext:self.managedObjectContext];
    
    
    [fetchRequest setEntity:desc];
    
    NSSortDescriptor* nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[nameDescriptor]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    _fetchedResultsController = aFetchedResultsController;
    return _fetchedResultsController;
}


-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    
    return  [sectionInfo name];
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath: (NSIndexPath*) indexPath {
    
    AHStudent* stud = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text =  [NSString stringWithFormat:@"%@ %@", stud.firstName, stud.lastName];
}



#pragma mark - UITableViewDelegate 



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AHStudent* student = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    AHStudentProfileController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"studentProfileController"];
    
    vc.student = student;
    
    [self showViewController:vc sender:self];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
