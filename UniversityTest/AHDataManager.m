//
//  AHDataManager.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHDataManager.h"

static NSString* firstNames[] = {
    @"Tran", @"Lenore", @"Bud", @"Fredda", @"Katrice",
    @"Clyde", @"Hildegard", @"Vernell", @"Nellie", @"Rupert",
    @"Billie", @"Tamica", @"Crystle", @"Kandi", @"Caridad",
    @"Vanetta", @"Taylor", @"Pinkie", @"Ben", @"Rosanna",
    @"Eufemia", @"Britteny", @"Ramon", @"Jacque", @"Telma",
    @"Colton", @"Monte", @"Pam", @"Tracy", @"Tresa",
    @"Willard", @"Mireille", @"Roma", @"Elise", @"Trang",
    @"Ty", @"Pierre", @"Floyd", @"Savanna", @"Arvilla",
    @"Whitney", @"Denver", @"Norbert", @"Meghan", @"Tandra",
    @"Jenise", @"Brent", @"Elenor", @"Sha", @"Jessie"
};

static NSString* lastNames[] = {
    
    @"Farrah", @"Laviolette", @"Heal", @"Sechrest", @"Roots",
    @"Homan", @"Starns", @"Oldham", @"Yocum", @"Mancia",
    @"Prill", @"Lush", @"Piedra", @"Castenada", @"Warnock",
    @"Vanderlinden", @"Simms", @"Gilroy", @"Brann", @"Bodden",
    @"Lenz", @"Gildersleeve", @"Wimbish", @"Bello", @"Beachy",
    @"Jurado", @"William", @"Beaupre", @"Dyal", @"Doiron",
    @"Plourde", @"Bator", @"Krause", @"Odriscoll", @"Corby",
    @"Waltman", @"Michaud", @"Kobayashi", @"Sherrick", @"Woolfolk",
    @"Holladay", @"Hornback", @"Moler", @"Bowles", @"Libbey",
    @"Spano", @"Folson", @"Arguelles", @"Burke", @"Rook"
};


@implementation AHDataManager

+(AHDataManager*) sharedManager {
    
    static AHDataManager* sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        sharedManager = [[AHDataManager alloc] init];
        
    });
    
    return sharedManager;
    
}

#pragma mark - get models from data base 

-(NSArray*) getAllStudents {
    
    NSError* error = nil;
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* desc = [NSEntityDescription entityForName:@"AHStudent"
                                            inManagedObjectContext:self.persistentContainer.viewContext];
    
    NSSortDescriptor* firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
    
    [request setSortDescriptors:@[firstNameDescriptor]];
    
    // [request setFetchBatchSize:20];
    //   [request setFetchLimit:35];
    //  [request setFetchOffset:10];
    // [request setResultType:NSDictionaryResultType];
    
    
    [request setEntity:desc];
    
    NSArray* results = [self.persistentContainer.viewContext executeFetchRequest:request error:&error];
    
    return results;
    
    
}

-(NSArray*) getStudentsInCourse:(AHCourse*) course withPredicate: (NSPredicate*) predicate {
    
    NSError* error = nil;
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* desc = [NSEntityDescription entityForName:@"AHStudent"
                                            inManagedObjectContext:self.persistentContainer.viewContext];
    
    [request setEntity:desc];
    
    NSSortDescriptor* firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
    
    [request setSortDescriptors:@[firstNameDescriptor]];
    
        
    [request setPredicate:predicate];
    
    NSArray* results = [self.persistentContainer.viewContext executeFetchRequest:request error:&error];
    
    return results;
    
    
}

-(NSArray*) getTeachersInCourse:(AHCourse*) course {
    
    NSError* error = nil;
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* desc = [NSEntityDescription entityForName:@"AHTeacher"
                                            inManagedObjectContext:self.persistentContainer.viewContext];
    
    [request setEntity:desc];
    
    NSSortDescriptor* firstNameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
    
    [request setSortDescriptors:@[firstNameDescriptor]];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"courses CONTAINS %@", course];
    
    [request setPredicate:predicate];
    
    NSArray* results = [self.persistentContainer.viewContext executeFetchRequest:request error:&error];
    
    return results;
    
    
}


#pragma mark - Create Model

-(void) generateStudentWithName:(NSString*) firstName lastName:(NSString*) lastName {
    
    
    AHStudent* student = [NSEntityDescription insertNewObjectForEntityForName:@"AHStudent" inManagedObjectContext:self.persistentContainer.viewContext];
    
    student.firstName = firstName;
    student.lastName  = lastName;
    student.eMail     = [NSString stringWithFormat:@"%@%@@mail.ru", student.firstName, student.lastName];
    
    NSError* error = nil;
    
    if (![self.persistentContainer.viewContext save:&error]) {
        
        NSLog(@"%@", [error localizedDescription]);
        
        
    }
    
}

-(AHTeacher*) generateRandomTeacherAndAddHimToCourse:(AHCourse*) course {
    
    
    AHTeacher* teacher = [NSEntityDescription insertNewObjectForEntityForName:@"AHTeacher" inManagedObjectContext:self.persistentContainer.viewContext];
    
    teacher.firstName = firstNames[arc4random_uniform(50)];
    teacher.lastName  = lastNames[arc4random_uniform(50)];
    
    [teacher addCoursesObject:course];
    
    
    NSError* error = nil;
    
    if (![self.persistentContainer.viewContext save:&error]) {
        
        NSLog(@"%@", [error localizedDescription]);
        
        
    }
    
    return teacher;
    
}

-(void) generateTeacherWithName:(NSString*) firstName lastName:(NSString*) lastName {
    
    
    AHTeacher* teacher = [NSEntityDescription insertNewObjectForEntityForName:@"AHTeacher" inManagedObjectContext:self.persistentContainer.viewContext];
    
    teacher.firstName = firstName;
    teacher.lastName  = lastName;
    
    NSError* error = nil;
    
    if (![self.persistentContainer.viewContext save:&error]) {
        
        NSLog(@"%@", [error localizedDescription]);
        
        
    }
    
}


-(void) generateCourseWithName:(NSString*) name andStudents: (NSSet*) students {
    
    
    AHCourse* course = [NSEntityDescription insertNewObjectForEntityForName:@"AHCourse" inManagedObjectContext:self.persistentContainer.viewContext];
    
    course.name = name;
    
    [course addStudents:students];
    
    NSError* error = nil;
    
    if (![self.persistentContainer.viewContext save:&error]) {
        
        NSLog(@"%@", [error localizedDescription]);
        
        
    }
    
}



#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"UniversityTest"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}


@end


