//
//  AHDataManager.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreData/CoreData.h>

#import "AHStudent+CoreDataClass.h"
#import "AHStudent+CoreDataProperties.h"

#import "AHCourse+CoreDataClass.h"
#import "AHCourse+CoreDataProperties.h"

#import "AHTeacher+CoreDataClass.h"
#import "AHTeacher+CoreDataProperties.h"

@interface AHDataManager : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;

+(AHDataManager*) sharedManager;

-(void) generateStudentWithName:(NSString*) firstName lastName:(NSString*) lastName;
-(void) generateCourseWithName:(NSString*) name andStudents: (NSSet*) students;
-(void) generateTeacherWithName:(NSString*) firstName lastName:(NSString*) lastName;
-(AHTeacher*) generateRandomTeacherAndAddHimToCourse:(AHCourse*) course;

-(NSArray*) getAllStudents;
-(NSArray*) getStudentsInCourse:(AHCourse*) course withPredicate: (NSPredicate*) predicate;
-(NSArray*) getTeachersInCourse:(AHCourse*) course;

@end
