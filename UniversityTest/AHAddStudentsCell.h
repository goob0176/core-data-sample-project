//
//  AHAddStudentsCell.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AHAddStudentsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *setStatOfStudentTextField;

@end
