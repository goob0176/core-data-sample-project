//
//  AHTeacher+CoreDataProperties.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHTeacher+CoreDataProperties.h"

@implementation AHTeacher (CoreDataProperties)

+ (NSFetchRequest<AHTeacher *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AHTeacher"];
}

@dynamic firstName;
@dynamic lastName;
@dynamic courses;

@end
