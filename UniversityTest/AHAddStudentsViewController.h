//
//  AHAddStudentsViewController.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AHStudent+CoreDataClass.h"
#import "AHStudent+CoreDataProperties.h"

@interface AHAddStudentsViewController : UITableViewController <UITextFieldDelegate>

@property (assign, nonatomic) BOOL editProfile;
@property (weak, nonatomic) AHStudent* student;



@end
