//
//  AHCoursesViewController.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHCoursesViewController.h"

#import "AHCourse+CoreDataClass.h"
#import "AHCourse+CoreDataProperties.h"

#import "AHStudentProfileController.h"
#import "AHAddStudentsViewController.h"
#import "AHCourseViewController.h"

#import "AHDataManager.h"

@interface AHCoursesViewController ()

@end

@implementation AHCoursesViewController

@synthesize fetchedResultsController = _fetchedResultsController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSFetchedResultsController*)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    // [[AHDataManager sharedManager] generateAndAddUniversuty];
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* desc = [NSEntityDescription entityForName:@"AHCourse"
                                            inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:desc];
    
    NSSortDescriptor* nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[nameDescriptor]];
    
    if (self.teacher) {
        
        
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"teacher == %@", self.teacher];
        
        [fetchRequest setPredicate:predicate];
        
    }
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    _fetchedResultsController = aFetchedResultsController;
    return _fetchedResultsController;
}


-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    
    return  [sectionInfo name];
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath: (NSIndexPath*) indexPath {
    
    AHCourse* course = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.textLabel.text =  [NSString stringWithFormat:@"%@", course.name];
}

#pragma mark - UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    AHCourse* course = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    AHCourseViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"courseViewController"];
    
    vc.course = course;
    
    [self showViewController:vc sender:self];
        

    
}

@end
