//
//  AHCourseViewController.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHCourseViewController.h"

#import "AHStudentProfileController.h"
#import "AHEditCourseViewController.h"

#import "AHDataManager.h"

@interface AHCourseViewController ()

@property (strong, nonatomic) NSMutableArray* studentsArray;
@property (strong, nonatomic) NSMutableArray* teachersArray;

@end

typedef enum {
    
    AHSectionNameTeacher,
    AHSectionNameStudent
    
    
} AHSectionName;

@implementation AHCourseViewController


-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    self.navigationItem.title = self.course.name;
    
    NSPredicate* predicate =  [NSPredicate predicateWithFormat:@"courses CONTAINS %@", self.course];
    
    
    self.studentsArray = [NSMutableArray arrayWithArray:[[AHDataManager sharedManager] getStudentsInCourse:self.course withPredicate:predicate]];
    self.teachersArray = [NSMutableArray arrayWithArray:[[AHDataManager sharedManager] getTeachersInCourse:self.course]];
    
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == AHSectionNameTeacher) {
        
        return @"Преподаватель";
        
    } else {
        
        return @"Студенты";
        
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if (section == AHSectionNameTeacher) {
        
        if (self.teachersArray.count == 0) {
            
            return 1;
            
            
        } else {
            
            return self.teachersArray.count;
            
        }
        
        
    } else {
        
            return self.course.students.count;
    }

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifier = @"studCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];

    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        
    }
    
    if (indexPath.section == AHSectionNameTeacher) {
        
        if (self.teachersArray.count == 0) {
            
            cell.textLabel.text = @"Добавить преподавателя";
            
        } else {
            
            AHTeacher* teacher = [self.teachersArray objectAtIndex:indexPath.row];
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", teacher.firstName, teacher.lastName];
        }
        
    } else {
        
        AHStudent* student  = [self.studentsArray objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
        
    }
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == AHSectionNameTeacher) {
        
        return NO;
        
    } else {
        
      return YES;
        
    }
        
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == AHSectionNameTeacher) {
        
        if (self.teachersArray.count == 0) {
            
            [self createTeacherWithAlertController];
            
            
        } else {
            
            AHTeacher* teacher = [self.teachersArray objectAtIndex:indexPath.row];
            
            NSLog(@"%@ %@", teacher.firstName, teacher.lastName);
            
        }
        
        
    } else {
        
        AHStudent* student = [self.studentsArray objectAtIndex:indexPath.row];
        
        AHStudentProfileController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"studentProfileController"];
        
        vc.student = student;
        
        [self showViewController:vc sender:self];
        
    }
    
    
    
}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == AHSectionNameStudent) {
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            NSError* error = nil;
            
            AHStudent* student = [self.studentsArray objectAtIndex:indexPath.row];
            
            [self.studentsArray removeObject:student];
            
            [self.course removeStudentsObject:student];
            
            if (![[AHDataManager sharedManager].persistentContainer.viewContext save:&error]) {
                
                NSLog(@"%@", [error localizedDescription]);
                
            }
        
            [self.tableView reloadData];
            
        }
        
        
    }
    
   
    
    
}


#pragma mark - Methods 

-(void) createTeacherWithAlertController {
    
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Добавить преподавателя" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* generateTeacherAction = [UIAlertAction actionWithTitle:@"Добавить преподавателя" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        AHTeacher* teacher = [[AHDataManager sharedManager] generateRandomTeacherAndAddHimToCourse:self.course];
                
        [self.teachersArray addObject:teacher];
        
        [self.tableView reloadData];
        
    }];
    
    UIAlertAction* canselAction = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil];
    
    [ac addAction:generateTeacherAction];
    [ac addAction:canselAction];
    
    [self presentViewController:ac animated:YES completion:nil];
    
    
}

#pragma mark - Navigation

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"editCourse"]) {
        
        
        UINavigationController* nav = [segue destinationViewController];
        
        AHEditCourseViewController* vc = [[nav viewControllers] firstObject];
        
        vc.course = self.course;
        
    }
    
    
}

@end
