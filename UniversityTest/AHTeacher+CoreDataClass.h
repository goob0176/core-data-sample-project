//
//  AHTeacher+CoreDataClass.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AHCourse;

NS_ASSUME_NONNULL_BEGIN

@interface AHTeacher : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "AHTeacher+CoreDataProperties.h"
