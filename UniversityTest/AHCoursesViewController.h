//
//  AHCoursesViewController.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHCoreDataViewController.h"

#import "AHTeacher+CoreDataClass.h"
#import "AHTeacher+CoreDataProperties.h"

@interface AHCoursesViewController : AHCoreDataViewController

@property (strong, nonatomic) AHTeacher* teacher;

@end
