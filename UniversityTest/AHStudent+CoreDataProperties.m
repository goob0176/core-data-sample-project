//
//  AHStudent+CoreDataProperties.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHStudent+CoreDataProperties.h"

@implementation AHStudent (CoreDataProperties)

+ (NSFetchRequest<AHStudent *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AHStudent"];
}

@dynamic eMail;
@dynamic firstName;
@dynamic lastName;
@dynamic courses;

@end
