//
//  AHStudentProfileController.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

#import "AHStudent+CoreDataClass.h"
#import "AHStudent+CoreDataProperties.h"

@interface AHStudentProfileController : UIViewController

@property (strong, nonatomic) AHStudent* student;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *mailLabel;


@end
