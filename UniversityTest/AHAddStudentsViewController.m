//
//  AHAddStudentsViewController.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHAddStudentsViewController.h"

#import "AHAddStudentsCell.h"

#import "AHDataManager.h"

#import "AHStudent+CoreDataClass.h"
#import "AHStudent+CoreDataProperties.h"

@interface AHAddStudentsViewController ()

@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;

@end


typedef enum {
    
    AHStatFirstName,
    AHStatLastName
    
  } AHStat;

@implementation AHAddStudentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 2;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifier = @"addStudentsCell";
    
    AHAddStudentsCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (!cell) {
        
        cell = [[AHAddStudentsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }

    
   // cell.setStatOfStudentTextField.text = @"";
    cell.setStatOfStudentTextField.tag = indexPath.row;
    cell.setStatOfStudentTextField.delegate = self;
    [cell.setStatOfStudentTextField addTarget:self
                                       action:@selector(textFieldDidChange:)
                             forControlEvents:UIControlEventEditingChanged];
    
    
    if (self.editProfile) {
        
        [self createPlaceholdersForCell:cell firstPlaceholder:self.student.firstName secondPlaceholder:self.student.lastName];
        
    } else {
        
        
        [self createPlaceholdersForCell:cell firstPlaceholder:@"Введите имя" secondPlaceholder:@"Введите фамилию"];
        
    }
    
    return cell;
}



-(void) textFieldDidChange:(UITextField*) sender {
    
    if (sender.tag == AHStatFirstName) {
        
        self.firstName = sender.text;
        
    } else if (sender.tag == AHStatLastName) {
        
        self.lastName = sender.text;
        
    }
    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
    
}

#pragma mark - Actions 

- (IBAction)createStudent:(UIBarButtonItem *)sender {
    
     NSCharacterSet *badSymbols = [NSCharacterSet characterSetWithCharactersInString:@"1234567890 :<>@*!?#$%%^&-_+=±;/|`"];
    
    if (!self.editProfile) {
        
        if ([self.firstName rangeOfCharacterFromSet:badSymbols].location == NSNotFound && [self.lastName rangeOfCharacterFromSet:badSymbols].location == NSNotFound && ![self.firstName isEqualToString:@""] && ![self.lastName isEqualToString:@""]) {
            
            [[AHDataManager sharedManager] generateStudentWithName:self.firstName
                                                          lastName:self.lastName];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else {
            
            [self createErrorAlert];
            
        }
        
    } else {
        
        if ([self.firstName rangeOfCharacterFromSet:badSymbols].location == NSNotFound && [self.lastName rangeOfCharacterFromSet:badSymbols].location == NSNotFound && ![self.firstName isEqualToString:@""] && ![self.lastName isEqualToString:@""]) {
        
        self.student.firstName = self.firstName;
        self.student.lastName  = self.lastName;
        self.student.eMail     = [NSString stringWithFormat:@"%@%@ @ mail.ru", self.firstName,self.lastName];
        
        [[AHDataManager sharedManager].persistentContainer.viewContext save:nil];
        
        [self dismissViewControllerAnimated:YES completion:nil];
            
        } else {
            
            [self createErrorAlert];
            
        }
        
    }
    
}


- (IBAction)canselButton:(UIBarButtonItem *)sender {

    [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - Methods 




-(void) createErrorAlert {
    
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Ошибка" message:[self errorMessageText] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* action = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleCancel handler:nil];

    [ac addAction:action];
    
    [self presentViewController:ac animated:YES completion:nil];
    
    
}

-(NSString*) errorMessageText {
    
    
    NSString* message;
    
    if (self.firstName == nil || [self.firstName isEqualToString:@""] || self.lastName == nil || [self.lastName isEqualToString:@""]) {
        
        message = @"Пожалуйста, заполните все поля!";
        
    } else {
        
        message = @"Используйте только буквы при заполнении строк";
                
    }
    
    return message;
    
}

-(void) createPlaceholdersForCell:(AHAddStudentsCell*) cell firstPlaceholder:(NSString*) firstPlaceholder secondPlaceholder:(NSString*) secondPlaceholder {
    
    if (cell.setStatOfStudentTextField.tag == AHStatFirstName) {
        
        cell.setStatOfStudentTextField.placeholder = firstPlaceholder;
        
    } else if (cell.setStatOfStudentTextField.tag == AHStatLastName) {
        
        cell.setStatOfStudentTextField.placeholder = secondPlaceholder;
        
    }
    
    
}


@end
