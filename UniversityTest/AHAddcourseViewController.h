//
//  AHAddcourseViewController.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AHAddcourseViewController : UITableViewController <UITextFieldDelegate>

- (IBAction)generateCourse:(UIBarButtonItem *)sender;
- (IBAction)dismissController:(UIBarButtonItem *)sender;


@end
