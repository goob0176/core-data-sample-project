//
//  AHCoreDataViewController.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AHCoreDataViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext     *managedObjectContext;

- (void)configureCell:(UITableViewCell *)cell atIndexPath: (NSIndexPath*) indexPath;


@end
