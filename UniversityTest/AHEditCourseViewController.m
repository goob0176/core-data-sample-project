//
//  AHEditCourseViewController.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHEditCourseViewController.h"

#import "AHDataManager.h"

@interface AHEditCourseViewController ()

@property (strong, nonatomic) NSMutableArray* teachersArray;
@property (strong, nonatomic) NSMutableArray* studentsInCourseArray;
@property (strong, nonatomic) NSMutableArray* studentsNotInCourseArray;

@end

    typedef enum {
        
       AHSectionTypeTeacher,
       AHSectionTypeStudentsInCourse,
       AHSectionTypeStydentsNotInCourse
        
        
    } AHSectionType;


@implementation AHEditCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getStudentsInCourse];
    [self getStudentsNotInCourse];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    
    if (section == AHSectionTypeTeacher) {
        
        return 1;
        
        
    } else if (section == AHSectionTypeStudentsInCourse) {
        
        return self.studentsInCourseArray.count;
        
    } else if (section == AHSectionTypeStydentsNotInCourse) {
                
        return self.studentsNotInCourseArray.count;
        
    }
    
    return 0;

}

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    if (section == AHSectionTypeTeacher) {
        
        return @"Преподаватель";
        
        
    } else if (section == AHSectionTypeStudentsInCourse) {
        
        return @"Студенты в курсе";
        
    } else if (section == AHSectionTypeStydentsNotInCourse) {
        
        return @"Записать на курс";
        
    }
    
    
    return @"";
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
   static NSString* identifier = @"editCourseCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    if (indexPath.section == AHSectionTypeTeacher) {
        
        if (self.course.teacher) {
            
            cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",self.course.teacher.firstName, self.course.teacher.lastName];
            
        } else {
            
            cell.textLabel.text = @"Добавить преподавателя";
        }
        
        
    } else if (indexPath.section == AHSectionTypeStudentsInCourse) {
        
        AHStudent* student = [self.studentsInCourseArray objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
       
        
    } else if (indexPath.section == AHSectionTypeStydentsNotInCourse) {
        
        AHStudent* student = [self.studentsNotInCourseArray objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
        
    }
    
    
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == AHSectionTypeStydentsNotInCourse) {
        
        AHStudent* student = [self.studentsNotInCourseArray objectAtIndex:indexPath.row];
        
        [self.studentsNotInCourseArray removeObject:student];
        
        [self.course addStudentsObject:student];
        
        [[AHDataManager sharedManager].persistentContainer.viewContext save:nil];
        
        [self getStudentsInCourse];
        [self getStudentsNotInCourse];
        
        [self.tableView reloadData];
        
    } else if (indexPath.section == AHSectionTypeTeacher) {
        
        
        if (!self.course.teacher) {
            
            [self createTeacherWithAlertController];
            
            
        }
        
    }
    
}

#pragma mark - Methods 

-(void) createTeacherWithAlertController {
    
    UIAlertController* ac = [UIAlertController alertControllerWithTitle:@"Добавить преподавателя" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* generateTeacherAction = [UIAlertAction actionWithTitle:@"Добавить преподавателя" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        AHTeacher* teacher = [[AHDataManager sharedManager] generateRandomTeacherAndAddHimToCourse:self.course];
        
        [self.teachersArray addObject:teacher];
        
        [self.tableView reloadData];
        
    }];
    
    UIAlertAction* canselAction = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil];
    
    [ac addAction:generateTeacherAction];
    [ac addAction:canselAction];
    
    [self presentViewController:ac animated:YES completion:nil];
    
    
}

-(void) getStudentsInCourse {
    
    NSPredicate* studentsInCoursepredicate =  [NSPredicate predicateWithFormat:@"courses CONTAINS %@", self.course];
    
    NSArray* arrayOfStudentsInCourse = [[AHDataManager sharedManager] getStudentsInCourse:self.course withPredicate:studentsInCoursepredicate];
    
    self.studentsInCourseArray = [NSMutableArray arrayWithArray:arrayOfStudentsInCourse];
    
    
}

-(void) getStudentsNotInCourse {
    
  //  NSPredicate* studentsNotInCoursepredicate =  [NSPredicate predicateWithFormat:@"SUBQUERY(courses, $course, $course.name != %@)", self.course];
    
    NSMutableArray* arrayOfStudentsNotInCourse = [[[AHDataManager sharedManager] getAllStudents] mutableCopy];
    
    for (AHStudent* student in [[AHDataManager sharedManager] getAllStudents]) {
        
        if ([self.studentsInCourseArray containsObject:student]) {
            
            [arrayOfStudentsNotInCourse removeObject:student];
        }
        
    }
    
    self.studentsNotInCourseArray = [NSMutableArray arrayWithArray:arrayOfStudentsNotInCourse];
    
}

#pragma mark - Actions

- (IBAction)dismissController:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.section != AHSectionTypeStudentsInCourse) {
        
        return NO;
        
    } else {
        
     return YES;
        
    }
    
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (indexPath.section == AHSectionTypeStudentsInCourse) {
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            
            AHStudent* student = [self.studentsInCourseArray objectAtIndex:indexPath.row];
            
            [self.course removeStudentsObject:student];
            
            [self.studentsInCourseArray removeObject:student];
            
            [[AHDataManager sharedManager].persistentContainer.viewContext save:nil];
            
            [self getStudentsInCourse];
            [self getStudentsNotInCourse];
            
            [tableView reloadData];
            
            
        }
        
        
    }
    
    
}




@end
