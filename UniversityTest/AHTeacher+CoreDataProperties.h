//
//  AHTeacher+CoreDataProperties.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHTeacher+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AHTeacher (CoreDataProperties)

+ (NSFetchRequest<AHTeacher *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *firstName;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, retain) NSSet<AHCourse *> *courses;

@end

@interface AHTeacher (CoreDataGeneratedAccessors)

- (void)addCoursesObject:(AHCourse *)value;
- (void)removeCoursesObject:(AHCourse *)value;
- (void)addCourses:(NSSet<AHCourse *> *)values;
- (void)removeCourses:(NSSet<AHCourse *> *)values;

@end

NS_ASSUME_NONNULL_END
