//
//  AHStudent+CoreDataProperties.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHStudent+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AHStudent (CoreDataProperties)

+ (NSFetchRequest<AHStudent *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *eMail;
@property (nullable, nonatomic, copy) NSString *firstName;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, retain) NSSet<AHCourse *> *courses;

@end

@interface AHStudent (CoreDataGeneratedAccessors)

- (void)addCoursesObject:(AHCourse *)value;
- (void)removeCoursesObject:(AHCourse *)value;
- (void)addCourses:(NSSet<AHCourse *> *)values;
- (void)removeCourses:(NSSet<AHCourse *> *)values;

@end

NS_ASSUME_NONNULL_END
