//
//  AHEditCourseViewController.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 07.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AHCourse+CoreDataClass.h"
#import "AHCourse+CoreDataProperties.h"


#import "AHStudent+CoreDataClass.h"
#import "AHStudent+CoreDataProperties.h"

#import "AHTeacher+CoreDataClass.h"
#import "AHTeacher+CoreDataProperties.h"

@interface AHEditCourseViewController : UITableViewController

@property (strong, nonatomic) AHCourse* course;

- (IBAction)dismissController:(UIBarButtonItem *)sender;


@end
