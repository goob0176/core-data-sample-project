//
//  AHStudentProfileController.m
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import "AHStudentProfileController.h"
#import "AHAddStudentsViewController.h"


@interface AHStudentProfileController ()


@end

@implementation AHStudentProfileController


-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    
    self.nameLabel.text = self.student.firstName;
    self.mailLabel.text = self.student.eMail;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"editProfile"]) {
        
        UINavigationController* nav = [segue destinationViewController];
        
        AHAddStudentsViewController* vc = [nav.viewControllers firstObject];
        
        vc.student = self.student;
        
        vc.editProfile = YES;
        
    }
    
    
}


@end
