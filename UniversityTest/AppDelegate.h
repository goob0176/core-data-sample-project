//
//  AppDelegate.h
//  UniversityTest
//
//  Created by Andrey Khlopotin on 06.06.17.
//  Copyright © 2017 Andrey Khlopotin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

